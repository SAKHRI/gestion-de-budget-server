<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;

class CategoriesFixtures extends Fixture
{
    public function load(ObjectManager $manager)  
    {

        for ($i=0; $i < 10; $i++) { 
            $category = new Category();
            $category->setName('category'. $i);
            $category->setImage('https://media.istockphoto.com/photos/red-apple-with-leaf-picture-id683494078?k=6&m=683494078&s=612x612&w=0&h=aVyDhOiTwUZI0NeF_ysdLZkSvDD4JxaJMdWSx2p3pp4=');
    
            $manager->persist($category);
        }
       

        $manager->flush();
    }
}
